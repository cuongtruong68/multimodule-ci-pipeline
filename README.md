# Multi-module Gitlab-ci pipeline

This is a quick example of how to handle CI pipelines for a multi-module project. It is illustrated with Scala/Maven simple project, but, of course, the idea could be implemented in other languages/package managers.

